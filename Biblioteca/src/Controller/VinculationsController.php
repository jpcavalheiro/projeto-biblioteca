<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Vinculations Controller
 *
 * @property \App\Model\Table\VinculationsTable $Vinculations
 *
 * @method \App\Model\Entity\Vinculation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VinculationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Books'],
        ];
        $vinculations = $this->paginate($this->Vinculations);

        $this->set(compact('vinculations'));
    }

    /**
     * View method
     *
     * @param string|null $id Vinculation id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vinculation = $this->Vinculations->get($id, [
            'contain' => ['Users', 'Books'],
        ]);

        $this->set('vinculation', $vinculation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vinculation = $this->Vinculations->newEntity();
        if ($this->request->is('post')) {
            $vinculation = $this->Vinculations->patchEntity($vinculation, $this->request->getData());
            if ($this->Vinculations->save($vinculation)) {
                $this->Flash->success(__('The vinculation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vinculation could not be saved. Please, try again.'));
        }
        $users = $this->Vinculations->Users->find('list', ['limit' => 200]);
        $books = $this->Vinculations->Books->find('list', ['limit' => 200]);
        $this->set(compact('vinculation', 'users', 'books'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Vinculation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vinculation = $this->Vinculations->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vinculation = $this->Vinculations->patchEntity($vinculation, $this->request->getData());
            if ($this->Vinculations->save($vinculation)) {
                $this->Flash->success(__('The vinculation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vinculation could not be saved. Please, try again.'));
        }
        $users = $this->Vinculations->Users->find('list', ['limit' => 200]);
        $books = $this->Vinculations->Books->find('list', ['limit' => 200]);
        $this->set(compact('vinculation', 'users', 'books'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Vinculation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vinculation = $this->Vinculations->get($id);
        if ($this->Vinculations->delete($vinculation)) {
            $this->Flash->success(__('The vinculation has been deleted.'));
        } else {
            $this->Flash->error(__('The vinculation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
