<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class ApiController extends AppController{   

    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->allow('add');
    }

    public function add()
    {
        $this->loadModel('Users');

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
            }
            echo $user;
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        exit;
    }
}
?>