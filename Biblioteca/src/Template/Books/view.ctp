<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Book $book
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar Livro'), ['action' => 'edit', $book->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar Livro'), ['action' => 'delete', $book->id], ['confirm' => __('Are you sure you want to delete # {0}?', $book->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Livros'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo Livro'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Usuários'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo Usuário'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Vinculações'), ['controller' => 'Vinculations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova Vinculação'), ['controller' => 'Vinculations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'login']) ?> </li>
    </ul>
</nav>
<div class="books view large-9 medium-8 columns content">
    <h3><?= h($book->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Título') ?></th>
            <td><?= h($book->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descrição') ?></th>
            <td><?= h($book->discription) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usuário') ?></th>
            <td><?= $book->has('user') ? $this->Html->link($book->user->name, ['controller' => 'Users', 'action' => 'view', $book->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($book->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Criado') ?></th>
            <td><?= h($book->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modificado') ?></th>
            <td><?= h($book->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Livros Vinculados') ?></h4>
        <?php if (!empty($book->vinculations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('ID') ?></th>
                <th scope="col"><?= __('Nome do Usuário') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($book->vinculations as $vinculations):?>
            <tr>
                <td><?= h($vinculations->id) ?></td>
                <td><?= $vinculations->user->name?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Detalhes'), ['controller' => 'Vinculations', 'action' => 'view', $vinculations->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Vinculations', 'action' => 'edit', $vinculations->id]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Vinculations', 'action' => 'delete', $vinculations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vinculations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
