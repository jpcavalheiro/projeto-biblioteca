<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vinculation $vinculation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Deletar'),
                ['action' => 'delete', $vinculation->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $vinculation->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar Vinculações'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Usuários'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Usuário'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Livros'), ['controller' => 'Books', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Livro'), ['controller' => 'Books', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'login']) ?> </li>
    </ul>
</nav>
<div class="vinculations form large-9 medium-8 columns content">
    <?= $this->Form->create($vinculation) ?>
    <fieldset>
        <legend><?= __('Editar Vinculação') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('book_id', ['options' => $books]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
