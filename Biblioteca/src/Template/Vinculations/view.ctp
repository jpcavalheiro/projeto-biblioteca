<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vinculation $vinculation
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar Vinculação'), ['action' => 'edit', $vinculation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Vinculação'), ['action' => 'delete', $vinculation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vinculation->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Vinculações'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova Vinculação'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Usuários'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo Usuário'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Livros'), ['controller' => 'Books', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo Livro'), ['controller' => 'Books', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'login']) ?> </li>
    </ul>
</nav>
<div class="vinculations view large-9 medium-8 columns content">
    <h3><?= h($vinculation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Usuário') ?></th>
            <td><?= $vinculation->has('user') ? $this->Html->link($vinculation->user->name, ['controller' => 'Users', 'action' => 'view', $vinculation->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Livro') ?></th>
            <td><?= $vinculation->has('book') ? $this->Html->link($vinculation->book->title, ['controller' => 'Books', 'action' => 'view', $vinculation->book->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($vinculation->id) ?></td>
        </tr>
    </table>
</div>
