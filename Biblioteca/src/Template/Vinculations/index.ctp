<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vinculation[]|\Cake\Collection\CollectionInterface $vinculations
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Nova Vinculação'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Usuários'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Usuário'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Livros'), ['controller' => 'Books', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Livro'), ['controller' => 'Books', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'login']) ?> </li>
    </ul>
</nav>
<div class="vinculations index large-9 medium-8 columns content">
    <h3><?= __('Vinculations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Nome do Usuário') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Nome do Livro') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($vinculations as $vinculation): ?>
            <tr>
                <td><?= $this->Number->format($vinculation->id) ?></td>
                <td><?= $vinculation->has('user') ? $this->Html->link($vinculation->user->name, ['controller' => 'Users', 'action' => 'view', $vinculation->user->id]) : '' ?></td>
                <td><?= $vinculation->has('book') ? $this->Html->link($vinculation->book->title, ['controller' => 'Books', 'action' => 'view', $vinculation->book->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $vinculation->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $vinculation->id]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $vinculation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vinculation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
